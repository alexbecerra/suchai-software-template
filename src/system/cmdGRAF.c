/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Alex Becerra, abecerra@ing.uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* NOTES
int rc = _com_send_data(node, data, len(data), type_of_telemetry, N_of_type (default= 1), seq_number (default = 0))
*/

#include "payload/cmdGRAF.h"

static const char *tag = "cmdGRAF";

void cmd_graf_init(void)
{
    cmd_add("graf_test", graf_test, "", 0);
    cmd_add("graf_reboot", graf_reboot, "", 0);
    cmd_add("graf_halt", graf_halt, "", 0);
    cmd_add("graf_start_pigpio", graf_start_pigpio, "", 0);
    cmd_add("graf_stop_pigpio", graf_stop_pigpio, "", 0);

    /*  Parameters:
        node: int
        vdrain: int
        vgate: int
        chans: int
        steps: int
        delta: int
        dtn: int
        samples: int
        file: int
        db: int
    */
    cmd_add("graf_take_data", graf_take_data, "%d %d %d %d %d %d %d %d %d %d", 10);

    /* Parameters:
        node: int
        number: int
    */
    cmd_add("graf_get_names", graf_get_names, "%d %d", 2);

    /* Parameters:
        node: int
        filename: long long
    */
    cmd_add("graf_get_file_comp", graf_get_file_comp, "%d %lld", 2);

    /* Parameters:
        node: int
        filename: long long
        first: int
        total: int
    */
    cmd_add("graf_get_file_comp_frames", graf_get_file_comp_frames, "%d %lld %d %d", 4);

     /* Parameters:
        node: int
        filename: long long
    */
    cmd_add("graf_get_file_text", graf_get_file_text, "%d %lld", 2);

    /* Parameters:
        node: int
        filename: long long
        first: int
        total: int
    */
    cmd_add("graf_get_file_text_frames", graf_get_file_text_frames, "%d %lld %d %d", 4);
}

int graf_test(char* fmt, char* params, int nparams)
{
    printf("--> GRAF: OK!\n");
    system("pwd");
    return CMD_OK;
}

int graf_reboot(char* fmt, char* params, int nparams)
{
    printf("--> GRAF: Reboot... \n");
    system("sudo reboot");
    return CMD_OK;
}

int graf_halt(char* fmt, char* params, int nparams)
{
    printf("--> GRAF: Halt... \n");
    system("sudo halt");
    return CMD_OK;
}

int graf_start_pigpio(char* fmt, char* params, int nparams)
{
    printf("--> GRAF: Starting PIGPIOD... \n");
    int ret = system("sudo pigpiod");
    if (ret == 0){
        return CMD_OK;
    }
    return CMD_FAIL;
}

int graf_stop_pigpio(char* fmt, char* params, int nparams)
{
    printf("--> GRAF: Stopping PIGPIOD... \n");
    int ret = system("sudo killall pigpiod");
    if (ret == 0){
        return CMD_OK;
    }
    return CMD_FAIL;
}

int graf_take_data(char* fmt, char* params, int nparams)
{
    int node;
    char task[SCH_CMD_MAX_STR_PARAMS];
    int vdrain;
    int vgate;
    int chans;
    int steps;
    int delta;
    int dtn;
    int samples;
    int file;
    int db;

    if(params == NULL || sscanf(params, fmt, &node, &vdrain, &vgate, &chans, &steps, &delta, &dtn, &samples, &file, &db) != nparams)
        return CMD_ERROR;
    /*
    snprintf(task, sizeof(task), "python3 ../stt_python/take_one_picture.py %s", exp_time);
    */
    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/FS-graf_transistor/take_data.py -vdrain %d -vgate %d -chans %d -steps %d -delta %d -dtn %d -samples %d -file %d -db %d &",
                vdrain, vgate, chans, steps, delta, dtn, samples, file, db);
    system(task);

    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), GRAF_RETURN_CODE, 1, 0);
    return CMD_OK;
}

int graf_get_names(char* fmt, char* params, int nparams)
{
    int node;
    int nfiles;
    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, &nfiles) != nparams)
        return CMD_ERROR;

    printf("--> GRAF: Getting filenames... \n");
    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/FS-graf_transistor/get_files.py -n %d", nfiles);
    system(task);

    long long names[nfiles];

    FILE *myfile;
    myfile = fopen("/home/pi/GRAF_last_files", "r");
    if (myfile == NULL)
        return CMD_FAIL;
        //exit(0);

    int j = 0;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    char *ptr;
    long ret;
    while ((read = getline(&line, &len, myfile)) != -1) {
        names[j] = strtoll(line, &ptr, 10);
        printf("--> GRAF: Name file is %lld\n", (long long) names[j]);
        j = j + 1;
    }
    fclose(myfile);

    int rc = _com_send_data(node, (void *) names, sizeof(names), GRAF_LIST_FILE, 1, 0);
    return CMD_OK;
}

int graf_get_file_comp(char* fmt, char* params, int nparams)
{

    int node;
    long long filename;
    char filepath[256];

    if(params == NULL || sscanf(params, fmt, &node, &filename) != nparams)
        return CMD_ERROR;

    int j = snprintf(filepath, 256, "/home/pi/graf-data/%lld.bz2", filename);
    printf("---> GRAF: File to read: %s\n", filepath);

    FILE *myfile;
    char *buffer;
    long filelen;
    myfile = fopen(filepath, "rb");
    if (myfile == NULL)
        return CMD_FAIL;

    fseek(myfile, 0, SEEK_END);
    filelen = ftell(myfile);
    rewind(myfile);

    buffer = (char *)malloc(filelen * sizeof(char));
    fread(buffer, filelen, 1, myfile);
    fclose(myfile);

    printf("File lenght: %d\n", filelen);
    printf("Size buffer: %d\n", sizeof(buffer));

    int i;
    for (i = 0; i < filelen; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", buffer[i]);
    }
    printf("\n");

    int rc = _com_send_data(node, (void *) buffer, filelen, GRAF_FILE, 1, 0);

    return CMD_OK;

}

int graf_get_file_comp_frames(char* fmt, char* params, int nparams)
{

    int node;
    long long filename;
    int first;
    int tframes;
    char filepath[256];

    if(params == NULL || sscanf(params, fmt, &node, &filename, &first, &tframes) != nparams)
        return CMD_ERROR;

    int j = snprintf(filepath, 256, "/home/pi/graf-data/%lld.bz2", filename);
    printf("---> GRAF: File to read: %s\n", filepath);

    FILE *myfile;
    char *buffer;
    long filelen;
    myfile = fopen(filepath, "rb");
    if (myfile == NULL)
        return CMD_FAIL;

    fseek(myfile, 0, SEEK_END);
    filelen = ftell(myfile);
    rewind(myfile);

    buffer = (char *)malloc(filelen * sizeof(char));
    fread(buffer, filelen, 1, myfile);
    fclose(myfile);

    printf("File lenght: %d\n", filelen);
    printf("Size buffer: %d\n", sizeof(buffer));

    int n_frames_full = filelen / BperF;
    if (filelen % BperF > 0) {
        n_frames_full++;
    }
    printf("File frames: %d\n", n_frames_full);

    // If frame to start is grater than the last frame, exit
    if ((first + 1) > n_frames_full){
        return CMD_ERROR;
    }

    // Check if first + tframes is larger than the actual available frames
    // Also, if -1 options is used for tframes, heck all available frames
    if ( (first + tframes) > (n_frames_full) || (tframes == -1)){
        tframes = n_frames_full - first;
    }

    // If tframes is zero, theres nothing to send
    if (tframes == 0){
        return CMD_ERROR;
    }

    printf("Start frame: %d\n", first);
    printf("Get T frames: %d\n", tframes);

    int i;
    for (i = 1; i <= filelen; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", buffer[i-1]);
        if (i % BperF == 0){
            printf("\n");
        }
    }
    printf("\n");

    char *buffer_partial;
    int partial_length = ((tframes) * (BperF + 2)) * sizeof(char);
    buffer_partial = (char *)malloc( partial_length);

    printf("Length partial buffer: %d\n", partial_length);


    int k, p, q, actual_pos;
    p = 0;

    for (k = first; k <= (first + tframes - 1); k++){
        buffer_partial[(k - first + p*(BperF + 1))] = ((k & 0xFF00) >> 8);
        buffer_partial[(k - first + p*(BperF + 1) + 1)] = (k & 0x00FF);
        for (q = 0; q < BperF; q++){
            actual_pos = k*BperF + q;
            if (actual_pos > filelen) {
                buffer_partial[(k - first + p * (BperF + 1) + (q + 2))] = 0x40;
            }
            else {
                buffer_partial[(k - first + p * (BperF + 1) + (q + 2))] = buffer[k * BperF + q];
            }
        }
        p++;
    }

    printf("===FRAMES===\n");
    int r;
    for (r = 1; r <= partial_length; r++)
    {
        printf(":");
        printf("%02X", buffer_partial[r - 1]);
        if (r % (BperPack_CSP) == 0 && r !=0) {
            printf("\n");
        }
    }
    printf("\n");

    int rc = _com_send_data(node, (void *) buffer_partial, partial_length, GRAF_COMP_FILE, 1, 0);
    return CMD_OK;
}

int graf_get_file_text(char* fmt, char* params, int nparams)
{

    int node;
    long long filename;
    char filepath[256];

    if(params == NULL || sscanf(params, fmt, &node, &filename) != nparams)
        return CMD_ERROR;

    int j = snprintf(filepath, 256, "/home/pi/graf-data/%lld", filename);
    printf("---> GRAF: File to read: %s\n", filepath);

    FILE *myfile;
    char *buffer;
    long filelen;
    myfile = fopen(filepath, "rb");
    if (myfile == NULL)
        return CMD_FAIL;

    fseek(myfile, 0, SEEK_END);
    filelen = ftell(myfile);
    rewind(myfile);

    buffer = (char *)malloc(filelen * sizeof(char));
    fread(buffer, filelen, 1, myfile);
    fclose(myfile);

    printf("File lenght: %d\n", filelen);
    printf("Size buffer: %d\n", sizeof(buffer));
    int i;
    for (i = 0; i < filelen; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", buffer[i]);
    }
    printf("\n");

    int rc = _com_send_data(node, (void *) buffer, filelen, GRAF_COMP_FILE, 1, 0);
    return CMD_OK;
}

int graf_get_file_text_frames(char* fmt, char* params, int nparams)
{

    int node;
    long long filename;
    int first;
    int tframes;
    char filepath[256];

    if(params == NULL || sscanf(params, fmt, &node, &filename, &first, &tframes) != nparams)
        return CMD_ERROR;

    int j = snprintf(filepath, 256, "/home/pi/graf-data/%lld", filename);
    printf("---> GRAF: File to read: %s\n", filepath);

    FILE *myfile;
    char *buffer;
    long filelen;
    myfile = fopen(filepath, "rb");
    if (myfile == NULL)
        return CMD_FAIL;

    fseek(myfile, 0, SEEK_END);
    filelen = ftell(myfile);
    rewind(myfile);

    buffer = (char *)malloc(filelen * sizeof(char));
    fread(buffer, filelen, 1, myfile);
    fclose(myfile);

    printf("File lenght: %d\n", filelen);
    printf("Size buffer: %d\n", sizeof(buffer));

    int n_frames_full = filelen / BperF;
    if (filelen % BperF > 0) {
        n_frames_full++;
    }
    printf("File frames: %d\n", n_frames_full);

    // If frame to start is grater than the last frame, exit
    if ((first + 1) > n_frames_full){
        return CMD_ERROR;
    }

    // Check if first + tframes is larger than the actual available frames
    // Also, if -1 options is used for tframes, heck all available frames
    if ( (first + tframes) > (n_frames_full) || (tframes == -1)){
        tframes = n_frames_full - first;
    }

    // If tframes is zero, theres nothing to send
    if (tframes == 0){
        return CMD_ERROR;
    }

    printf("Start frame: %d\n", first);
    printf("Get T frames: %d\n", tframes);

    int i;
    for (i = 1; i <= filelen; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", buffer[i-1]);
        if (i % BperF == 0){
            printf("\n");
        }
    }
    printf("\n");

    char *buffer_partial;
    int partial_length = ((tframes) * (BperF + 2)) * sizeof(char);
    buffer_partial = (char *)malloc( partial_length);

    printf("Length partial buffer: %d\n", partial_length);


    int k, p, q, actual_pos;
    p = 0;

    for (k = first; k <= (first + tframes - 1); k++){
        buffer_partial[(k - first + p*(BperF + 1))] = ((k & 0xFF00) >> 8);
        buffer_partial[(k - first + p*(BperF + 1) + 1)] = (k & 0x00FF);
        for (q = 0; q < BperF; q++){
            actual_pos = k*BperF + q;
            if (actual_pos > filelen) {
                buffer_partial[(k - first + p * (BperF + 1) + (q + 2))] = 0x40;
            }
            else {
                buffer_partial[(k - first + p * (BperF + 1) + (q + 2))] = buffer[k * BperF + q];
            }
        }
        p++;
    }

    printf("===FRAMES===\n");
    int r;
    for (r = 1; r <= partial_length; r++)
    {
        printf(":");
        printf("%02X", buffer_partial[r - 1]);
        if (r % (BperPack_CSP) == 0 && r !=0) {
            printf("\n");
        }
    }
    printf("\n");

    int rc = _com_send_data(node, (void *) buffer_partial, partial_length, GRAF_COMP_FILE, 1, 0);
    return CMD_OK;

}
