/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Alex Becerra, abecerra@ing.uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* NOTES

int rc = _com_send_data(node, data, len(data), type_of_telemetry, N__of_type (default= 1), seq_number (default = 0))

*/

#include "payload/cmdPLANT.h"

static const char *tag = "cmdPLANT";

void cmd_plant_init(void)
{

    cmd_add("plant_test", plant_test, "", 0);

    /* Parameters:
        node: int
        width: int
        height: int
        qual: int
        sharp: int
        contrast: int
        br: int
        sa: int
        iso: int
    */
    cmd_add("plant_take_pic", plant_take_pic, "%d %d %d %d %d %d %d %d %d", 9);

    /* Parameters:
        node: int
        number: int
    */
    cmd_add("plant_get_names", plant_get_names, "%d %d", 2);

    /* Parameters:
        node: int
        filename: long long
    */
    cmd_add("plant_get_file", plant_get_file, "%d %lld", 2);

    /* Parameters:
        node: int
        filename: long long
        first: int
        total: int
    */
    cmd_add("plant_get_file_frames", plant_get_file_frames, "%d %lld %d %d", 4);
}

int plant_test(char* fmt, char* params, int nparams)
{
    printf("--> PLANT: OK!\n");
    system("pwd");
    return CMD_OK;
}

int plant_take_pic(char* fmt, char* params, int nparams)
{
    char task[SCH_CMD_MAX_STR_PARAMS];
    int node;
    int w;
    int h;
    int q;
    int sh;
    int co;
    int br;
    int sa;
    int iso;

    if(params == NULL || sscanf(params, fmt, &node, &w, &h, &q, &sh, &co, &br, &sa, &iso) != nparams)
        return CMD_ERROR;
    /*
    snprintf(task, sizeof(task), "python3 ../stt_python/take_one_picture.py %s", exp_time);
    */
    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/FS-graf_transistor/plant_get_photo.py -width %d -height %d -qual %d -sharp %d -contrast %d -br %d -sa %d -iso %d",
                w, h, q, sh, co, br, sa, iso);
    system(task);

    int rc = _com_send_data(node, (void *) CMD_OK, sizeof(CMD_OK), PLANT_RETURN_CODE, 1, 0);
    return CMD_OK;
}

int plant_get_names(char* fmt, char* params, int nparams)
{
    int node;
    int nfiles;

    char task[SCH_CMD_MAX_STR_PARAMS];

    if(params == NULL || sscanf(params, fmt, &node, &nfiles) != nparams)
        return CMD_ERROR;

    printf("--> PLANT: Getting filenames... \n");
    snprintf(task, sizeof(task), "python3 /home/pi/suchai-software-template/FS-graf_transistor/plant_get_names.py -n %d", nfiles);
    system(task);

    long long names[nfiles];

    FILE *myfile;
    myfile = fopen("/home/pi/PLANT_last_files", "r");
    if (myfile == NULL)
        return CMD_FAIL;

    int j = 0;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    char *ptr;
    long ret;
    while ((read = getline(&line, &len, myfile)) != -1) {
        names[j] = strtoll(line, &ptr, 10);
        printf("--> PLANT: Name file is %lld\n", (long long) names[j]);
        j = j + 1;
    }
    fclose(myfile);

    int rc = _com_send_data(node, (void *) names, sizeof(names), PLANT_LIST_FILE, 1, 0);
    return CMD_OK;
}

int plant_get_file(char* fmt, char* params, int nparams)
{

    int node;
    long long filename;
    char filepath[256];

    if(params == NULL || sscanf(params, fmt, &node, &filename) != nparams)
        return CMD_ERROR;

    int j = snprintf(filepath, 256, "/home/pi/plant-data/%lld", filename);
    printf("---> PLANT: File to read: %s\n", filepath);

    FILE *myfile;
    char *buffer;
    long filelen;
    myfile = fopen(filepath, "rb");
    if (myfile == NULL)
        return CMD_FAIL;

    fseek(myfile, 0, SEEK_END);
    filelen = ftell(myfile);
    rewind(myfile);

    buffer = (char *)malloc(filelen * sizeof(char));
    fread(buffer, filelen, 1, myfile);
    fclose(myfile);

    printf("File lenght: %d\n", filelen);
    printf("Size buffer: %d\n", sizeof(buffer));
    int i;
    for (i = 0; i < filelen; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", buffer[i]);
    }
    printf("\n");

    int rc = _com_send_data(node, (void *) buffer, filelen, PLANT_FILE, 1, 0);

    return CMD_OK;

}

int plant_get_file_frames(char* fmt, char* params, int nparams)
{

    int node;
    long long filename;
    int first;
    int tframes;
    char filepath[256];

    if(params == NULL || sscanf(params, fmt, &node, &filename) != nparams)
        return CMD_ERROR;

    int j = snprintf(filepath, 256, "/home/pi/plant-data/%lld", filename);
    printf("---> PLANT: File to read: %s\n", filepath);

    FILE *myfile;
    char *buffer;
    long filelen;
    myfile = fopen(filepath, "rb");
    if (myfile == NULL)
        return CMD_FAIL;

    fseek(myfile, 0, SEEK_END);
    filelen = ftell(myfile);
    rewind(myfile);

    buffer = (char *)malloc(filelen * sizeof(char));
    fread(buffer, filelen, 1, myfile);
    fclose(myfile);

    printf("File lenght: %d\n", filelen);
    printf("Size buffer: %d\n", sizeof(buffer));

    int n_frames_full = filelen / BperF;
    if (filelen % BperF > 0) {
        n_frames_full++;
    }
    printf("File frames: %d\n", n_frames_full);

    // If frame to start is grater than the last frame, exit
    if ((first + 1) > n_frames_full){
        return CMD_ERROR;
    }

    // Check if first + tframes is larger than the actual available frames
    // Also, if -1 options is used for tframes, heck all available frames
    if ( (first + tframes) > (n_frames_full) || (tframes == -1)){
        tframes = n_frames_full - first;
    }

    // If tframes is zero, theres nothing to send
    if (tframes == 0){
        return CMD_ERROR;
    }

    printf("Start frame: %d\n", first);
    printf("Get T frames: %d\n", tframes);

    int i;
    for (i = 1; i <= filelen; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", buffer[i-1]);
        if (i % BperF == 0){
            printf("\n");
        }
    }
    printf("\n");

    char *buffer_partial;
    int partial_length = ((tframes) * (BperF + 2)) * sizeof(char);
    buffer_partial = (char *)malloc( partial_length);

    printf("Length partial buffer: %d\n", partial_length);


    int k, p, q, actual_pos;
    p = 0;

    for (k = first; k <= (first + tframes - 1); k++){
        buffer_partial[(k - first + p*(BperF + 1))] = ((k & 0xFF00) >> 8);
        buffer_partial[(k - first + p*(BperF + 1) + 1)] = (k & 0x00FF);
        for (q = 0; q < BperF; q++){
            actual_pos = k*BperF + q;
            if (actual_pos > filelen) {
                buffer_partial[(k - first + p * (BperF + 1) + (q + 2))] = 0x40;
            }
            else {
                buffer_partial[(k - first + p * (BperF + 1) + (q + 2))] = buffer[k * BperF + q];
            }
        }
        p++;
    }

    printf("===FRAMES===\n");
    int r;
    for (r = 1; r <= partial_length; r++)
    {
        printf(":");
        printf("%02X", buffer_partial[r - 1]);
        if (r % (BperPack_CSP) == 0 && r !=0) {
            printf("\n");
        }
    }
    printf("\n");

    int rc = _com_send_data(node, (void *) buffer_partial, partial_length, PLANT_FILE, 1, 0);
    return CMD_OK;

}