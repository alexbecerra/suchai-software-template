/**
 * @file  cmdGRAF.h
 * @author Alex Becerra - abecerra@ing.uchile.cl
 * @date 2021
 * @copyright GNU Public License.
 *
 * This header contains commands related with the tests leds
 */

#ifndef SUCHAI_FLIGHT_SOFTWARE_CMDGRAF_H
#define SUCHAI_FLIGHT_SOFTWARE_CMDGRAF_H

#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "stdint.h"

#include "repoCommand.h"
#include "os/os.h"

/* NOTE: Define telemetry type
Assign from 20 to 29
*/
#define GRAF_RETURN_CODE 20
#define GRAF_LIST_FILE 21
#define GRAF_FILE 22
#define GRAF_COMP_FILE 23

#define BperPack_CSP    129
#define BperF           127

/**
 * Registers graf commands in the system
 */
void cmd_graf_init(void);

int graf_test(char* fmt, char* params, int nparams);
int graf_reboot(char* fmt, char* params, int nparams);
int graf_halt(char* fmt, char* params, int nparams);

int graf_start_pigpio(char* fmt, char* params, int nparams);
int graf_stop_pigpio(char* fmt, char* params, int nparams);

int graf_take_data(char* fmt, char* params, int nparams);
int graf_get_names(char* fmt, char* params, int nparams);
int graf_get_file_comp(char* fmt, char* params, int nparams);
int graf_get_file_comp_frames(char* fmt, char* params, int nparams);
int graf_get_file_text(char* fmt, char* params, int nparams);
int graf_get_file_text_frames(char* fmt, char* params, int nparams);

#endif /* SUCHAI_FLIGHT_SOFTWARE_CMDGRAF_H */