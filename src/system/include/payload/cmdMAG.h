/**
 * @file  cmdMAG.h
 * @author Alex Becerra - abecerra@ing.uchile.cl
 * @date 2021
 * @copyright GNU Public License.
 *
 * This header contains commands related with the tests leds
 */

#ifndef SUCHAI_FLIGHT_SOFTWARE_CMDMAG_H
#define SUCHAI_FLIGHT_SOFTWARE_CMDMAG_H

#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "stdint.h"

#include "repoCommand.h"
#include "os/os.h"

/* NOTE: Define telemetry type
Assign from 24 to 27
*/
#define MAG_RETURN_CODE 24
#define MAG_LIST_FILE 25
#define MAG_FILE 27

#define BperPack_CSP    129
#define BperF           127


void cmd_mag_init(void);

int mag_test(char* fmt, char* params, int nparams);
int mag_send_inst(char* fmt, char* params, int nparams);
int mag_get_eeprom(char* fmt, char* params, int nparams);
int mag_get_names(char* fmt, char* params, int nparams);
int mag_get_file_text(char* fmt, char* params, int nparams);
int mag_get_file_text_frames(char* fmt, char* params, int nparams);

#endif /* SUCHAI_FLIGHT_SOFTWARE_CMDMAG_H */