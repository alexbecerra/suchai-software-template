/**
 * @file  cmdPLANT.h
 * @author Alex Becerra - abecerra@ing.uchile.cl
 * @date 2021
 * @copyright GNU Public License.
 *
 * This header contains commands related with the tests leds
 */

#ifndef SUCHAI_FLIGHT_SOFTWARE_CMDPLANT_H
#define SUCHAI_FLIGHT_SOFTWARE_CMDPLANT_H

#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "stdint.h"

#include "repoCommand.h"
#include "os/os.h"

/* NOTE: Define telemetry type
Assign from 24 to 27
*/
#define PLANT_RETURN_CODE 28
#define PLANT_LIST_FILE 29
#define PLANT_FILE 27

#define BperPack_CSP    129
#define BperF           127


void cmd_plant_init(void);

int plant_test(char* fmt, char* params, int nparams);
int plant_take_pic(char* fmt, char* params, int nparams);
int plant_get_names(char* fmt, char* params, int nparams);
int plant_get_file(char* fmt, char* params, int nparams);
int plant_get_file_frames(char* fmt, char* params, int nparams);

#endif /* SUCHAI_FLIGHT_SOFTWARE_CMDPLANT_H */